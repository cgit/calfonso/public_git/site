Name:           genome-site
Version:        1.0.0
Release:        14%{?dist}
Summary:        Genome web site 

Group:          Applications/System
License:        GPL 
URL:            http://genome-repo.usersys.redhat.com 
Source0:        %{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

Requires:       httpd
Requires:       php

%description
Basic web site for genome

%prep
%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/var/www/genome
mkdir -p $RPM_BUILD_ROOT/etc/httpd/conf.d

cp -r genome-site/content/* $RPM_BUILD_ROOT/var/www/genome
cp genome.conf $RPM_BUILD_ROOT/etc/httpd/conf.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(755,root,root)
/etc/httpd/conf.d/genome.conf
/var/www/genome

%changelog
* Mon Jul 21 2008 Jeroen van Meeuwen <kanarip@fedoraproject.org> - 1.0.0-14
- Initial packaging for Fedora Review
