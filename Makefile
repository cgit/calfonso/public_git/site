NAME 		:= genome-site
SPECFILE	= ${NAME}.spec
VERSION         = $(shell rpm -q --qf "%{VERSION}\n" --specfile $(SPECFILE)| head -1)
RELEASE         = $(shell rpm -q --qf "%{RELEASE}\n" --specfile $(SPECFILE)| head -1)

TAG             = $(subst .,_,${NAME}-${VERSION}-$(RELEASE))

CVS             = cvs

ARCHIVE         = ${NAME}-${VERSION}.tar.gz	

rpm: clean dist
	@rpmbuild -ta $(ARCHIVE)

srpm: clean dist
	@rpmbuild -ts $(ARCHIVE)

clean:
	@rm -rfv *~ *.rpm $(ARCHIVE)

dist:
	@rm -rf ${NAME}-${VERSION}.tar.gz
	@rm -rf /tmp/${NAME}-${VERSION} /tmp/${NAME}
	@dir=$$PWD; cd /tmp; cp -a $$dir ${NAME}
	@mv /tmp/${NAME} /tmp/${NAME}-${VERSION}
	@dir=$$PWD; cd /tmp; tar --exclude .git -czvf $$dir/${NAME}-${VERSION}.tar.gz ${NAME}-${VERSION}
	@rm -rf /tmp/${NAME}-${VERSION}
	@echo "The archive is in ${NAME}-${VERSION}.tar.gz"

