<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<? include("header.php") ?>
  <div id="body">
<? include("menu.php") ?>
        <div id="content">
            <div id="inner">
               <h1>Download The Source!</h1>
               <ul class="toc">

                  <li><a href="http://fedorahosted.org/genome">The Genome project is hosted on fedorahosted.org</a>
                  <ul>
                    <li><a href="../docs/genome/#genome-ContributeCodeCheckout">Checkout The Code</a></li>
                  </ul>
                  </li>
                </ul>
            </div>
        </div>
  </div>
<? include("footer.php") ?>

