<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<? include("header.php") ?>
  <div id="body">
<? include("menu.php") ?>
    <div id="content">
        <div id="inner">
	    <div id="doc">
		<h2>Screenshots</h2>
		<p>
			<ul class="screenshots">
				<li><span class="screenshot-desc">The Cloud web interface (http://[CLOUD_SERVER]/cloud/status.html)</span><a href="styles/Cloudmaster_large.png" ><img src="./styles/Cloudmaster_small.png" /></a></li>
				<li><span class="screenshot-desc">The Genome nodes web interface (http://[GENOME_SERVER/genome/nodes.html])</span><a href="styles/Genomed_nodes_large.png" ><img src="./styles/Genomed_nodes_small.png" /></a></li>
				<li><span class="screenshot-desc">The Genome node web interfaace (http://[GENOME_SERVER/genome/nodes/[MACHINE_NAME].html])</span><a href="styles/Genomed_node_large.png" ><img src="./styles/Genomed_node_small.png" /></a></li>
			</ul>
		</p>
	      </div>
        </div>
    </div>
  </div>
<? include("footer.php") ?>

