<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<? include("header.php") ?>
  <div id="body">
<? include("menu.php") ?>
    <div id="content">
        <div id="inner">
	    <div id="doc">
		<h2>Genome overview:</h2>
		<p>Genome at a high level is just a set of tools that allow you to provision an Genome repository machine, which in turn maintains the configuration of cloud based infrastructure machines.
			Whether you need a cluster of JBoss Applications Servers, replicated MySQL databases, or an Apache proxy tier, the Genome project allows you to consistently provision and configure entire IT system tiers.
			Get started with building an Genome based infrastructure! Read the <a href="../docs/genome">Genome Documentation</a>.</p>
		<h3>Create an isolated infrastructure using the Genome tools</h3>
                <p>Use the genome tools to create an Genome Repository machine and a cloud of machines to build out an entire IT infrastructure.</p>
		<p><img src="./styles/genome-high-level.png" border="0" alt="genome high level diagram" /></p>
		<h3>Setup parallel infrastructure and synchronize later </h3>
		<p>Multiple Genome repositories can share a cloud of machines.  Creating isolated Genome repositories allows a team to isolate their changes from others and then later synchronize changes when features are ready to merge.</p>
		<p><img src="./styles/genome-expansion.png" border="0" alt="genome high level diagram" /></p>
    	    
	      </div>
        </div>
    </div>
  </div>
<? include("footer.php") ?>

