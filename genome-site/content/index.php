<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<? include("header.php") ?>
  <div id="body">
<? include("menu.php") ?>
<div id="wrapper">
  <div id="content">
      <div id="inner">
		<h1>Genome: IT Evolved.</h1>
		<p>Genome is both a set of light weight tools and framework for introducing new server configurations and replicating those server configurations from one environment to another.</p>  

		<p>Changing systems manually in production can be very dangerous. Making changes in a completely different environment then installing those changes in production can be equally risky.  Using Genome to spin up an identical copy of a production system can help mitigate the risk of change.</p>

		<p>Large teams of developers struggle to move code and configuration into production without having a staging area that reflects the true state of production. The Genome framework can be used to manage the delta between staging and development environments by using consistent provisioning and configuration.</p>

		<p>Once a system has been installed into production, it's important to maintain that system over time.  This can be very difficult if there is no reliable way to re-create that system outside of the environment it's installed in.  Using Genome can provide visibility to both system administrators and developers into how systems are deployed.  If you create configuration manifests for a system, you can reliably rebuild the system in an isolated environment, or you can even use it to rebuild the system in production.</p>

		<p>In addition to cataloging system configurations, Genome also provides a basic cloud-based computing solution.  The Genome Cloud is a service that can deploy virtual machines across 1-to-many physical hosts.  Genome wires together some of the best open source infrastructure management projects.</p>
		<p><img src="./styles/genome_stack.png" border="0" alt="Genome uses cobbler, puppet, func, kvm, and xen" /></p>
     </div>  
  </div>
</div>
<? include("footer.php") ?>

