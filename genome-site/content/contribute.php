<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<? include("header.php") ?>
  <div id="body">
<? include("menu.php") ?>
        <div id="content">
            <div id="inner">
               <h1>Making Genome Better</h1>
               <ul class="toc">

                  <li><a href="../docs/genome/#genome-Contribute">Introduction</a>
                  <ul>
                    <li><a href="../docs/genome/#genome-ContributeLicensing">Licensing</a></li>
                    <li><a href="../docs/genome/#genome-ContributeDesign">Design Axioms</a></li>
                  </ul>
                  </li>

                  <li><a href="../docs/genome/#genome-ContributeCommunity">The Genome Community</a>
                  <ul>
                    <li><a href="../docs/genome/#genome-ContributeCommunityFriendly">Please Be Friendly</a></li>
                    <li><a href="../docs/genome/#genome-ContributeCommunityDiscuss">Where to Discuss Genome</a></li>
                  </ul>
                  </li>

                  <li><a href="../docs/genome/#genome-ContributeWorkWithCode">Working with the Code</a>
                  <ul>
                    <li><a href="../docs/genome/#genome-ContributeCodeCheckout">Checking out the Source</a></li>
                  </ul>
                  </li>
                  <li><a href="https://fedorahosted.org/genome/roadmap">The Genome Roadmap</a></li>
                </ul>
		<p>
                  <h3>Open a defect</h3>
                  If you have a fedora project account, you can <a href="https://fedorahosted.org/genome/report">open a Trac ticket</a>
		</p> 
                <p>
                  <h3>Mailing list</h3>
                  Join our mailing list by filling out this <a href="https://www.redhat.com/mailman/listinfo/genome-list">form</a>
                </p>
            </div>
        </div>
  </div>
<? include("footer.php") ?>

