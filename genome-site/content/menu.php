<?php
    define("HOME",    "h");
    define("DOWNLOAD",    "g");
    define("CONTRIBUTE",    "c");
    define("OVERVIEW",  "o");
    define("SCREENSHOTS",  "s");
	define("ACTIVE", "active");
	define("INACTIVE", "inactive");
        $trk = $_REQUEST['trk'];

	$home = $trk == HOME || $trk == '' ? 'active' : 'inactive';
	$download = $trk == DOWNLOAD ? 'active' : 'inactive';
	$contribute = $trk == CONTRIBUTE ? 'active' : 'inactive';
	$overview = $trk == OVERVIEW ? 'active' : 'inactive';
	$screenshots = $trk == SCREENSHOTS ? 'active' : 'inactive';
	
?>
<div id="nav-menu">
   <ul class="l0">
	<li><div>         <a class="<?php echo $home ?>" title="Home" href="./index.php">Home</a>        </div></li>
	<li><div>         <a class="<?php echo $download ?>" title="Get the latest releases" href="./download.php?trk=g">Download</a>        </div></li>
	<li><div>         <a class="<?php echo $contribute ?>" title="Information about design/development process" href="./contribute.php?trk=c">Contribute</a>      </div></li>
	<li><div>         <a class="inactive" title="Project documentation" href="../docs/genome">Documentation</a>       </div></li>
	<li><div>         <a class="<?php echo $overview ?>" title="Overview" href="./overview.php?trk=o">Overview</a>       </div></li>
	<li><div>         <a class="inactive" title="Quickstart" href="https://fedorahosted.org/genome/wiki/QuickStart">Quickstart</a>       </div></li>
	<li><div>         <a class="<?php echo $screenshots ?>" title="Screenshots" href="./screenshots.php?trk=s">Screenshots</a>       </div></li>
	<li><div>         <a class="inactive" title="Wiki" href="https://fedorahosted.org/genome/wiki">Wiki</a>       </div></li>
   </ul>
</div>
